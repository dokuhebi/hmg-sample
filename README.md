# Source for the HMG Website

This is the source for the HMG Website.  

## Contributing

1. Open a new issue
2. Submit a code for merge request

## Building the website

The website uses Hugo to build.  Pull the source and run "hugo server" to test the website locally.
