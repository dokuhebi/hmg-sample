---
title: "Resources"
date: 2021-06-26T14:51:12+06:00
author: HMG Team
image_webp: images/pixel.webp
image: images/pixel.png
description : "Resources"
---

*Disclaimer: The links are provided are for informational purposes only and do not indicate an endorsement from Hold My Guns.*

## Crisis Hotlines

[National Suicide Prevention Lifeline (Website/Online Chat)](https://suicidepreventionlifeline.org/) Dial "988"

[Veterans Crisis Line (Website/Online Chat)](https://www.veteranscrisisline.net/) Dial "988" and Press 1 or text "838255" for Veterans, Service Members and Family Members

## Organizations

[National Shooting Sports Foundation (NSSF) / American Foundation for Suicide Prevention (AFSP) Partnership](https://www.nssf.org/safety/suicide-prevention/)

[Project ChildSafe](https://projectchildsafe.org/) (Cable locks and safety resources)

[Walk the Talk America](https://walkthetalkamerica.org/)

[Doctors for Responsible Gun Ownership: Connect to a Pro-Rights Practitioner](https://drgo.us/2adoc-com-will-connect-patients-and-providers/)

## Friends of HMG
[RedBallon](https://www.redballoon.work/) Use discount Code "holdmyguns" 10% off ANY Package, including custom employee screening services.

[Retention Ring](https://retentionring.com/) In collaboration with Annette Evans of On Her Own, $1 for every "Acid Purple" Retention Ring sold will go to the organization Hold My Guns to support gun owners in times of mental health crisis or personal need!

[POM Industries](https://pompepperspray.com/)