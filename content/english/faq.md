---
title: "Frequently Asked Questions"
date: 2021-05-22T14:51:12+06:00
author: HMG Team
image_webp: images/faq/faq.webp
image: images/faq/faq.jpg
description : "Frequently Asked Questions"
---

## General

### What is Hold My Guns?

Hold My Guns&reg; is a liberty-based nonprofit organization. We partner with FFLs across the United States to provide voluntary firearms storage during times of crisis or personal need. Founded by gun owners, Hold My Guns® is a non-legislative solution to help save lives, protect property, and preserve rights.

### Are you “gun grabbers” in disguise?

No, but thanks for asking. We believe in the preservation of individual rights, and we’d be upset with you if you didn’t do your due diligence to ask.

### Can Hold My Guns be used by prohibited individuals?

No. Customers using Hold My Guns referral service must attest at the time of storage that they can lawfully possess a firearm and are not prohibited. Storing firearms for prohibited individuals is outside of the scope of Hold My Guns’ referral service.  

While it is outside of the scope of Hold My Guns, some FFLs may privately offer safekeeping services.  In any situation where there a court order involved, a customer should consult with their attorney to ensure that the safekeeping arrangement meets the requirements as determined by the presiding judge and is done in accordance with  federal, state, and local laws.

As per the storage agreement, customers who become prohibited from possessing a firearm while a firearm is being stored through the Hold My Guns referral service must notify the FFL and may opt to sell the firearm to the FFL or transfer the firearm to a non-prohibited third party in accordance with federal, state, and local laws.

### What if a customer does not pass a background check?

Customers who believe they mistakenly have not passed a background check may file a [NICS Voluntary Appeal](https://www.fbi.gov/file-repository/vaf-form-25.pdf/view).

## For Clients

### What are some of the reasons gun owners would choose to store firearms off-site?

First of all, we don’t ask why gun owners are storing their firearms.  That’s their personal business.  However, practical scenarios might include preventing unauthorized access to firearms during the sale of a home, while away on vacation, while a household member or guest is prohibited from being around firearms, when providing emergency housing to an at-risk foster child, during military deployment, etc. Other scenarios might include during volatile times such as during a divorce, when a household member who normally would have access to firearms is experiencing depression, or when a household member is going through a mental health or medical treatment that would increase their risk for harming themselves or harming others.

### Why would anyone want a gun shop to store firearms for them, instead of a trusted friend or family member?

If you have a friend or family member who can lawfully and responsibly store firearms for you in your times of need, and isn’t a gossip, you’re blessed. Give them a hug next time you see them for being so awesome. Not everyone is so blessed. Supporters of Hold My Guns&reg; recognize that while they personally may not need this option under their current circumstances, that having this service available is one that benefits our community as a whole.

## Storage Partners

### Are storage clients required to complete a 4473?

Customers are required to complete a 4473 when they store items classified as firearms. However, storage of nonserialized accessories and critical parts of the firearm (e.g. barrel, firing pin) do not require a 4473 to be completed upon return unless specified by state or local laws.

### Does a Report of Multiple Sale of Revolvers or Pistols form need to be filled out when multiple firearms are stored and returned to the owner?

Form ATF E-Form 3310.4 states, “This form is not required when the pistols or revolvers are returned to the same person from whom they are received.”
