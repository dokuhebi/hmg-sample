---
title: "Sarah Joy Albrecht"
image: "images/team/sarah.jpg"
email: "sarah@holdmyguns.org"
social:
#  - icon : "ti-facebook" # themify icon pack : https://themify.me/themify-icons
#    link : "#"
#  - icon : "ti-twitter-alt" # themify icon pack : https://themify.me/themify-icons
#    link : "#"
  - icon : "ti-linkedin" # themify icon pack : https://themify.me/themify-icons
    link : "https://www.linkedin.com/in/sarahjoyalbrecht/"
---

Sarah is a Range Safety Officer who enjoys helping new shooters get acclimated. Prior to founding HMG, she served as doula and childbirth educator for 11 years. She loves the Lord and her family. She is not a fan of root beer.
