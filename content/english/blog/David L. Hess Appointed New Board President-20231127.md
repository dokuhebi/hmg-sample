---
title: "David L. Hess Appointed New Board President of Nonprofit Hold My Guns®"
date: 2023-11-27T14:51:12+06:00
author: Hold My Guns 
image_webp: images/team/David_hess.webp
image: images/team/David_hess.jpg
#description : "This is meta description"
---

**For Immediate Release**

Hold My Guns® (HMG), a 501(c)(3) nonprofit organization dedicated to supporting gun owners through firearm storage, is pleased to announce that the Board has appointed David L. Hess as the new Board President.

This transition comes in response to the organization’s co-founder, Genevieve Jones, taking a step back from an Officer’s position as President for personal reasons. HMG extends heartfelt thanks to Jones for her dedicated service as Board President and appreciates her willingness to continue serving on the Board in an advisory capacity.

*“To me, Hold My Guns® means freedom,” said Jones.  “When times of personal crisis are met with compassionate solutions, and not with force, that is when we have freedom. When we have the choice to take accountability for our own lives and make personal decisions that better our future, that is freedom.   Hold My Guns® not only provides a non-legislative solution for gun storage, but a community of like-minded people to go along with it.  Empowering people to choose to take their life back – that is what  Hold My Guns® does, and that is true freedom. In my new advisory role, it is my honor to continue to support this mission.”*

David L. Hess is a co-owner of HMG’s Arizona Storage Partner, MAD Partners Inc., and works as a Director for KPMG, LLP.  He has been involved with HMG for several months as an FFL Storage Partner and serving as a Board Member with a focus on FFL Engagement.  With his 22 years of experience in consulting, four years owning FFLs in Arizona and Tennessee, and being an NRA Certified Instructor, he brings a wealth of experience and knowledge to the HMG organization.  

Hess states, *“I am excited to help lead Hold My Guns® and continue the strong foundation that Genevieve has initiated.  I am developing strategic quarterly goals that include increased awareness, increased FFL partnerships, and funding through community donations.  I look forward to meeting our supporters at SHOT Show in January.”*

Under Hess’ leadership, HMG is planning for substantial growth over the next year.  To accomplish this, the organization is asking that community members prayerfully consider supporting its mission at <https://www.givesendgo.com/holdmygunsorg>.

**About Hold My Guns®:**
Holdmyguns.org - Hold My Guns® (HMG) is a 501(c)(3) nonprofit organization that connects gun owners with partnering Federal Firearms Licensees (FFLs) to provide voluntary storage during times of personal need. This non-legislative solution plays a vital role in preventing suicide, theft, instances of stolen firearms used in crimes, and negligent injuries due to unauthorized access. HMG's efforts help to save lives, protect property, and preserve rights. This work is made possible through generous donations. EIN: 84-3099819

Contact: <info@holdmyguns.org>

[Resource links](https://holdmyguns.org/resources/)