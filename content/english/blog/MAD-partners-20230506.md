---
title: "Arizona Firearms Retailer MAD Partners and Nonprofit Hold My Guns Join Forces to Provide Voluntary Storage Solution"
date: 2023-05-06T12:34:56+06:00
author: HMG Media
#image_webp: images/blog/meghna.webp
image: images/portfolio/MAD-Partners-Logo.jpg
description : "This is meta description"
---
## For Immediate Release

Hold My Guns® (HMG), a 501(c)(3) nonprofit organization dedicated to supporting gun owners through firearms storage, is pleased to announce its strategic partnership with MAD Partners Inc, a licensed firearms retailer serving the west valley of Phoenix, Arizona. This collaboration aims to support individuals in need of temporary firearm storage while aligning with MAD Partners' dedication to providing exceptional customer service and education to the local firearm community.

"In challenging situations like home sales, military deployments, providing housing to a foster child, or when someone in the home is facing personal struggles, it's essential for gun owners to have peace of mind, knowing that their firearms are securely stored and protected from unauthorized access," said Sarah Joy Albrecht, Founder and Executive Director of HMG. "We understand that not everyone has friends or family who can provide storage, and we are grateful for MAD Partners' service to help fulfill this crucial need."

Joining a growing Hold My Guns® family of storage locations, MAD Partners is the inaugural partner in the state of Arizona. Individuals seeking to store firearms, firearm parts like firing pins, or firearm accessories such as keys to gun locks or safes at this location are encouraged to visit madpartnersinc.com for store hours and contact information.

"MAD Partners has decided to partner with HMG to support those in need of temporary firearm storage,” said David Hess, co-owner, on behalf of the MAD Partner team. “This effort aligns with our Mission Statement to support rights protected by the Second Amendment and provide the highest level of customer service.  We understand that there may be times that an individual feels that a firearm should not be in their house, for whatever reason, and we are here to support them during those times by providing a temporary storage solution through our partnership with HMG.”

This partnership, formed following a conversation at NSSF’s SHOT Show in January 2023, highlights the commitment of MAD Partners Inc and Hold My Guns® to deliver valuable resources and support to firearm owners. By leveraging their expertise and shared values, they strive to enhance the firearm community and foster responsible practices.


**About MAD Partners, Inc:**
[Madpartnersinc.com](https://www.madpartnersinc.com/) - MAD Partners, Inc is a leading firearm retailer based in the west valley of Phoenix, Arizona. Committed to supporting the Second Amendment, MAD Partners Inc provides exceptional customer service and education to both experienced and new firearm owners. Their mission is to be the preferred firearm retailer in the local community, offering competitive prices and comprehensive services.


## Media Contact
Press Contact:
David Hess
Co-owner
<david@madpartnersinc.com>


**About Hold My Guns®:**
[Holdmyguns.org](https://holdmyguns.org/) - Hold My Guns® (HMG) is a 501(c)(3) nonprofit organization that connects gun owners with partnering Federal Firearms Licensees (FFLs) to provide voluntary storage during times of personal need. This non-legislative solution plays a vital role in preventing suicide, theft, instances of stolen firearms used in crimes, and negligent injuries due to unauthorized access. HMG's efforts help to save lives, protect property, and preserve rights. This work is made possible through the generous donations of supporters of the Mission.

Press Contact:
Sarah Joy Albrecht 
Founder, Executive Director
<sarah@holdmyguns.org>