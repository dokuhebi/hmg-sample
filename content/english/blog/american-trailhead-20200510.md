---
title: "Hold My Guns® Expands Firearms Storage Partner Network with American Trailhead™, LLC"
date: 2022-05-10T12:34:56+06:00
author: HMG Media
#image_webp: images/blog/meghna.webp
image: images/pixel.png
description : "This is meta description"
---
## For Immediate Release

(Holdmyguns.org) -  Effective immediately American TrailheadTM, LLC,  located in Cumming, GA, is offering voluntary firearms storage in partnership with 501(c)(3) nonprofit Hold My Guns®. Customers desiring to store firearms at, American TrailheadTM, LLC,  are asked to arrange an appointment via the contact form via <https://americantrailhead.com>.

Hold My Guns® (HMG), a for-gun-owners by-gun-owners organization, connects gun owners with Federal Firearms Licensees (FFLs) across the United States to provide storage during times of crisis or personal need such as during deployment or the sale of a home. The storage referral service is a non-legislative solution to help prevent suicide, negligent injuries, theft of firearms, and instances of stolen firearms used in crimes.

“American Trailhead™, LLC, is the fifth firearms dealer to join the growing number of Hold My Guns® locations, which brings us to an important 10% milestone in our efforts to offer firearms storage in all 50 states. We welcome them to our family of partners,” said Sarah Joy Albrecht, Founder and Executive Director of Hold My Guns®. “Our firearms community has held a longstanding tradition of promoting self-governance,  personal responsibility, and the protection and enjoyment of life. Our storage program is a natural extension of these values.”

Curating outdoor experiences for novices and enthusiasts alike, through providing a wide range of quality gear and excellent customer service, is a cornerstone of the American Trailhead™ brand.  With the guiding principle of treating everyone with respect and dignity, they purpose themselves to go the extra mile to help equip customers for their unique journeys. Offering professional firearms storage is yet another way American Trailhead™ demonstrates that they care about their customers.

“People from all walks of life are navigating complicated situations, many times not of their choosing,” said American Trailhead™, LLC,  Co-founders Gerry Summey, CEO, and Michael Dutch Summey, President, in a joint statement.  “American Trailhead™, LLC is proud to partner with Hold My Guns to bring a new private-sector solution to help people in crisis.  As HMG's first FFL Storage Partner in Georgia, together we can help keep people and property safe by offering voluntary firearms storage to the public and front-line professionals. We are strongly committed to the U.S. Constitution and Bill of Rights, including Americans’ private property and 2A rights.  Deescalating a temporary personal situation and preventing even just one death or suicide is a tremendous success. God has certainly blessed our family and business, and we are honored to utilize our resources in a way that will save lives. We take great satisfaction from working in an industry with like-minded people that go beyond normal business activities to have a positive impact on individuals, families, and communities.”

For more information on how Hold My Guns® saves lives, protects property, and preserves rights, and to support the mission, visit <https://holdmyguns.org>.

## Media Contact

Sarah Joy Albrecht  
Founder and Executive Director  
<https://holdmyguns.org>  
<media@holdmyguns.org>  

**About Hold My Guns®** - <https://holdmyguns.org>

Hold My Guns® connects gun owners with partnering Federal Firearms Licensees (FFLs) to provide voluntary storage during times of crisis or personal need. The service is a non-legislative solution to help prevent suicide, theft, instances of stolen firearms used in crimes, and negligent injuries due to unauthorized access. Hold My Guns’® mission is to save lives, protect property, and preserve rights.
