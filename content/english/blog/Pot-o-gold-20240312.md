---
title: "Leading Firearms Brands Unite for Pot of Gold's Charity Auction Benefiting Hold My Guns Nonprofit"
date: 2024-03-12T14:51:12+06:00
author: HMG Media
#image_webp: images/blog/meghna.webp
image: images/blog/hmg-potogold.jpg
description : "This is meta description"
---

**For Immediate Release**

March 12, 2024 - Nonprofit Hold My Guns, with gratitude, announces that Pot of Gold Auctions is hosting an online charity auction, March 12 through March 26, to raise funds in support of its mission. The liberty-based 501(c)(3) forges strategic alliances with licensed firearms retailers nationwide, to offer a secure offsite storage solution. Last year, this initiative facilitated the storage of 144 firearms through its voluntary program.

Several leading brands and individuals within the firearms community are contributing to the auction, including SIG Sauer, Mantis, Magpul, PHLstr, Guns.com, The Gun Collective, Blackout Coffee, HK, Charlie Cook, LiveFire App, Olympian Lexi Lagan, Arctic Freeze Co., Dana Loesch, Jeff Sperandeo, Blackbelt Ammunition, and Barnes Bullets.

SIG Sauer led as the inaugural sponsor of the auction, offering a variety of firearms up for bids, notably featuring the MCX-Spear LT Semi-Automatic Rifle among their contributions.

 "Sig Sauer's commitment to excellence in our firearms community extends beyond our range of products and training programs. We love to equip Americans toward self-governance and personal responsibility,” said Tom Taylor, SIG Sauer’s Chief Marketing Officer and Executive Vice President. “We recognize and support nonprofit Hold My Guns' work with firearms retailers to provide an option of voluntary, temporary, offsite storage to those who need this service. We're proud to support the community through Pot of Gold’s charity auction event, benefitting Hold My Guns' mission."

Sarah Joy Albrecht, Founder and Executive Director of Hold My Guns, emphasized the importance of community collaboration for achieving the mission's goals.

“The enthusiastic participation of these leaders in our firearms community sends a powerful message of care and support to firearm owners seeking voluntary storage, while also adding excitement and fun to the event,” said Albrecht. “We're profoundly grateful to Pot of Gold for orchestrating this auction, which provides needed financial support to expand to 40 locations across 26 states this year, given our budget goals are met. This initiative not only uplifts our mission but also fosters a sense of unity and friendly competition within the firearms community. I'm eager to see which brands will spark the most intense bidding wars. Wishing the best of luck to all our participants — let’s make this an event to remember!"

Pot Of Gold Auctions is a full-service auction house offering nationwide online auctions and world-class customer service. Pot Of Gold was founded in 1995 by the husband and wife team of Dan and Cheryl Todd who bring over 60 years of combined industry experience. They specialize in firearms, antiques, vintage, collectibles, jewelry, art, coin-op, and unique hard to find items.

“Pot Of Gold Auctions is honored to exclusively bring you the fundraising auction
for Hold My Guns,” said Cassie Todd-Jameson on behalf of the auction house. “We are grateful for the work they do, and we hope bidders are not only excited to bid on the amazing items from generous donors but also feel good knowing 100% of the proceeds go straight to Hold My Guns to help them continue their incredible work.”

To explore the items available for bidding and support Hold My Guns, visit [Pot of Gold Auctions](https://potofgoldestate.hibid.com/catalog/516930/march-26th-hold-my-guns-fundraiser-auction). 

## Guns.com:
[![Guns.com](http://img.youtube.com/vi/MbpSaEjDleg/0.jpg)](https://www.youtube.com/watch?v=MbpSaEjDleg)

## Charlie Cook GunGram & Riding Shotgun with Charlie: 
[![Charlie Cook GunGram & Riding Shotgun with Charlie](http://img.youtube.com/vi/mrvXoE6nP70/0.jpg)](https://www.youtube.com/watch?v=mrvXoE6nP70)

## Sarah Joy Albrecht, QPR Suicide Prevention Instructor:
[![Sarah Joy Albrecht, QPR Suicide Prevention Instructor](http://img.youtube.com/vi/t0VTJudfU64/0.jpg)](https://www.youtube.com/watch?v=t0VTJudfU64)

## Lisa Ludwig, Arctic Freeze Co. & She Trains You
[![Lisa Ludwig, Arctic Freeze Co. & She Trains You](http://img.youtube.com/vi/Fz7dPVIG7DA/0.jpg)](https://www.youtube.com/watch?v=Fz7dPVIG7DA)

*"Magpul is an industry leader when it comes to equipping our firearms community. That includes providing veterans, service members, and first responders with the most absolutely reliable products that support their professional and personal lives. Our sponsorship of the Hold My Guns charity auction goes beyond mere support—it's a bold statement of our commitment to facilitating access to vital resources such as voluntary firearms storage. This effort underscores our dedication to promoting personal responsibility and fostering positive, life-affirming decisions."*  - **Carol Bethea, Magpul** 

*“I am a firearms Instructor and actually have a whole portion of my CCW training dedicated to HMGs. I think it is needed here in NY and wish someone here would become a vendor. Anything I can do for HMGs I would love to assist.”* - **Lisa Ludwig, Arctic Freeze Co. & She Trains You**

*“We believe strongly in the mission of HMG, and we feel that voluntary suicide reduction programs are the way forward.”* - **Sarah Hauptman, PHLster Holsters**

*“Hold My Guns fulfills a need in the firearms community that isn't always thought of, but is always urgent when needed. Suicide Prevention in particular is a message that is close to my heart. Being able to promote Hold My Guns as a resource for those who need a safe storage option for those times is important to me.”* - **Olympian Lexi Lagan**

*“I personally have gone through some rough times battling mental illness, and shooting sports saved my life. To be able to advocate for other people struggling with the same things has been a blessing in my life. I want HMG to succeed, and not only myself, but the founder of Blackbelt, Irvin Gill, want to do everything we can to help the organization succeed.”* - **Genevieve Jones, Blackbelt Ammunition**

*“We are proud to support this truly worthy cause. Hold My Guns provides exactly the type of self-governance, and empowerment that when needed frees people to take care of whatever is happening in their lives without fear of repercussions for doing the right thing”* - **Grant Lessard, Mantis**

*“HMG’s is a pure and noble mission. It’s society's job to break the stigma and it starts with organizations and HMG is trailblazing to save lives”* - **Jeff Sperandeo**

*“I am proud to support Hold My Gun’s efforts in meeting needs, facilitating help, and offering solutions without further government involvement — all while respecting constitutional rights.”* - **Dana Loesch**

## Media Contact

Sarah Joy Albrecht  
Founder and Executive Director  
<https://holdmyguns.org>  
<media@holdmyguns.org>  


**About Hold My Guns®** - <https://holdmyguns.org>

Hold My Guns® connects gun owners with partnering Federal Firearms Licensees (FFLs) to provide voluntary storage during times of crisis or personal need. The service is a non-legislative solution to help prevent suicide, theft, instances of stolen firearms used in crimes, and negligent injuries due to unauthorized access. Hold My Guns’® mission is to save lives, protect property, and preserve rights. 