---
title: "Washington Gun Shop Partners with Hold My Guns® to Offer Voluntary Firearms Storage"
date: 2022-05-06T12:34:56+06:00
author: HMG Media
#image_webp: images/blog/meghna.webp
image: images/pixel.png
description : "This is meta description"
---
**For Immediate Release**

(Holdmyguns.org) - Effective immediately, Randall Family Firearms, LLC,  located 30 miles north of Seattle, in Everett, WA,  is offering voluntary firearms storage through the for-gun-owners by-gun-owners nonprofit, Hold My Guns®. 

Customers desiring to store firearms at Randall Family Firearms, LLC,  are asked to contact owners Jeremy and Lisa Randall via their website, [randallfamilyfirearms.com](https://randallfamilyfirearms.com/), and arrange a service appointment.

Hold My Guns® (HMG) connects gun owners with Federal Firearms Licensees (FFLs) across the United States to provide storage during times of crisis or personal need. The storage referral service, which empowers personal responsibility,  is a solution to help prevent suicide, negligent injuries, theft of firearms and instances of stolen firearms used in crimes. Hold My Guns® partners are required to follow all federal, state, and local laws pertaining to the transfer of firearms.  

Randall Family Firearms, LLC (RFF) joins a [growing number of HMG storage partners](/locations/), which include [St. Bernard Indoor Shooting Center](https://www.stbisc.com/) in Louisiana, [KC Small Arms](https://www.kcsmallarms.com/) in Massachusetts, [Branson Cerakote and Laser](https://bransoncerakote.com/) in Missouri, and a soon-to-be announced FFL partner in Georgia. 

“While many support entities share well-intentioned safety advice to securely store firearms offsite during the sale of a home or military deployment, or while going through difficult personal situations such as postpartum depression or a divorce, not everyone has friends or family members who are qualified to help with storage,” said Sarah Joy Albrecht, Hold My Guns®’ Founder and Executive Director. “Randall Family Firearms, LLC, combines the family-like care their namesake represents, plus the professional firearms services their customers already trust. We are grateful for their leadership to use their resources to provide this life-saving service to gun owners in Washington state.” 

With a long-standing family history of public service, the Randalls see running a firearms business as an extension of their values of helping to save lives and prevent people from becoming victims. Jeremy's father, Bob Randall, was a Department of Human and Social Services Educator (DSHS) for 36 years.  After he passed away, a training room was named in his honor to commemorate his passion for education. Lisa’s late father,  Dick Gagnon, served as a Homicide/Cold Case Detective in Seattle, for over 44 years, and his work was featured in the TV show [Forensic Files](https://tubitv.com/tv-shows/447828/s12-e07-the-day-the-music-died).

“When a cry for help was noticed our fathers were always there to extend a hand, something we greatly value,” said Lisa Randall. “At Randall Family Firearms,, we believe everyone is deserving of support, tools for success, and respect for their rights. From meeting basic needs to offering solutions and everything in between, we hope to carry on our fathers' legacies and extend that helping hand.  We are humbled at the opportunity to partner with Hold My Guns®, to create this space in the PNW to continue their mission." 

In addition to firearms sales and storage, Randall Family Firearms, LLC,  provides a variety of firearms-related services including firearms safety and tactical training, gunsmithing, and customizations such as cerakote finishing. Randall Family Firearms, LLC,  is also known for their offering of Mantis firearms training products, which affix to firearms and help to increase shot accuracy through tech-driven feedback. 

For more information on how Hold My Guns® saves lives, protects property, and preserves rights, and to support the mission, visit <https://holdmyguns.org>.


## Media Contact

Sarah Joy Albrecht  
Founder and Executive Director  
<https://holdmyguns.org>  
<media@holdmyguns.org>  


**About Hold My Guns®** - <https://holdmyguns.org>

Hold My Guns® connects gun owners with partnering Federal Firearms Licensees (FFLs) to provide voluntary storage during times of crisis or personal need. The service is a non-legislative solution to help prevent suicide, theft, instances of stolen firearms used in crimes, and negligent injuries due to unauthorized access. Hold My Guns’® mission is to save lives, protect property, and preserve rights. 