---
title: "St. Bernard Indoor Shooting Center Offers Voluntary Firearms Storage in Partnership with Nonprofit Hold My Guns®"
date: 2021-06-07T14:51:12+06:00
author: HMG Media
#image_webp: images/blog/meghna.webp
image: images/pixel.png
description : "This is meta description"
---

(June 7, 2021) –  Effective immediately, [St. Bernard Indoor Shooting Center](https://www.stbisc.com/), located at 212 Aycock St., Arabi, LA, is offering voluntary firearms storage through Hold My Guns®, a liberty-based nonprofit that connects gun owners with Federal Firearms Licensees (FFLs) across the United States to provide storage during times of crisis or personal need. Customers desiring to store firearms at St. Bernard Indoor Shooting Center are asked to call (504) 982-5433 or email info@stbisc.com to arrange an appointment.

Founded by gun owners, Hold My Guns® is a non-legislative solution to help prevent suicide, theft, instances of stolen firearms used in crimes, and accidental shootings. Partnering gun shops make a difference in their communities by utilizing their firearms storage space to help save lives. STBISC in Louisiana is the second FFL to officially partner with the nonprofit to offer firearms storage; the first was KC Small Arms in Massachusetts.

“Everything we do at STBISC stems from a core belief in the US Constitution and the general principles of freedom and liberty. To that end, there has to be a way for people to store guns safely and confidently in times of need with minimal opportunity for red tape and governmental intrusion — HMG is that option. Owned by a veteran and employing mostly veterans, STBISC has a firm understanding and an absolute belief in the rights of Americans and looks forward to helping those who need to trust someone to take care of their guns while they take care of themselves.”– Brannon LeBouef, STBISC Owner, USMC veteran

"It was through the high recommendation of leaders of a veterans suicide prevention program that I learned about Brannon LeBouef, Owner of St. Bernard Indoor Shooting Center. Re-acclimation into civilian life after deployment is a significant risk factor for suicidality – a complex concern that can be hard to address.  When I heard about Brannon’s successful job skills training program for veterans,  I reached out to learn more about his work and his firearms business. From the moment I first spoke with Brannon, who served in the U.S. Marine Corps for over 11 years, it was apparent how much he values his students and customers. Partnering with Brannon to bring the Hold My Guns® voluntary firearms storage program to Louisiana is a decision I feel confident about.  He is already making a difference in his community, and providing this life-saving service is simply a practical extension of his honorable character.” – Sarah Joy Albrecht, Founder and President of Hold My Guns®

Gun owners have a variety of personal reasons, including preventing unauthorized access to firearms while away on vacation or during military deployment, as to why they would choose to store their firearms outside of their homes.  However, not everyone has a friend or family member who can lawfully help with firearms storage. This service is a life-saving option that fills this gap.

For more information on Hold My Guns®, visit [holdmyguns.org](https://holdmyguns.org).
