---
title: "Pittsburgh-Area Gun Shop Offers Voluntary Firearms Storage in Partnership with Nonprofit Hold My Guns®"
date: 2022-12-11T12:34:56+06:00
author: HMG Media
#image_webp: images/blog/meghna.webp
image: images/portfolio/Allegheny_Arms.png
description : "This is meta description"
---
## For Immediate Release

(Holdmyguns.org) - Effective immediately Allegheny Arms and Gun Works (AAGW),  Bethel Park, PA,  is offering voluntary firearms storage in partnership with 501(c)(3) nonprofit Hold My Guns®.  Those desiring to store firearms at this location are encouraged to visit [alleghenyarms.com](https://alleghenyarms.com/) for store hours and contact information.

Hold My Guns® (HMG), a for-gun-owners by-gun-owners organization, connects gun owners with Federal Firearms Licensees (FFLs) across the United States. The storage referral service is a non-legislative solution to help prevent suicide, negligent injuries, theft of firearms, and stolen firearms used in crimes. As not everyone has friends or family who are able to store firearms during times of personal need, such as during deployment, the sale of a home, or if a household member temporarily needs distance from lethal means, this service helps to fill the gap.

“Allegheny Arms and Gun Works is known for having knowledgeable, professional staff who help customers to feel at home,”  said Sarah Joy Albrecht, Founder and Executive Director of Hold My Guns®. “Treating customers with dignity and respect makes all the difference, especially when gun owners are seeking to voluntarily store their personal firearms. It was not a surprise to me to recently learn that AAGW was awarded the [Military Friendly® Employer designation](https://www.militaryfriendly.com/is-allegheny-arms-gun-works-military-friendly/), an honor that is only bestowed after a rigorous qualifying process. They truly are top-notch.  It is with gratitude that we announce veteran-owned Allegheny Arms as the first HMG Storage Partner in Pennsylvania.”

In addition to a well-stocked retail showroom, AAGW offers professional gunsmithing covering everything from basic repairs on classic firearms to helping create one-of-a-kind custom carry or competition pistol builds.

“Prior to opening Allegheny Arms, I had the privilege of serving in the United States Air Force as a firefighter and EMT for eight years. This was followed by several years as a government contractor in both Afghanistan and Iraq. This exposure has made me keenly aware of just how real the elevated suicide rate is both on active duty and within the veteran community,” said Joshua Rowe,  Partner and Lead Gunsmith at Allegheny Arms & Gun Works.  

“Restrictive possession and transfer regulations make it more difficult to store firearms outside of the home and increase the general stigmatization of those going through mental health issues,” said Rowe. “Enter HMG. Sarah and her team not only found a legal way to temporarily, and more importantly, voluntarily secure guns from at-risk homes, but she and her team work tirelessly to improve the public perception surrounding firearms ownership and mental health.  Allegheny Arms reached out to HMG as soon as we became aware of the services they were offering. We are very pleased to be the first HMG partner in greater Pittsburgh and in Pennsylvania. We genuinely look forward to making a real impact on the veteran and firearm community at large.”

For more information on how Hold My Guns® saves lives, protects property, and preserves rights, and to support the mission, visit <https://holdmyguns.org>.

## Media Contact

Sarah Joy Albrecht  
Founder and Executive Director  
<https://holdmyguns.org>  
<media@holdmyguns.org>  

**About Hold My Guns®** - <https://holdmyguns.org>

Hold My Guns® connects gun owners with partnering Federal Firearms Licensees (FFLs) to provide voluntary storage during times of crisis or personal need. The service is a non-legislative solution to help prevent suicide, theft, instances of stolen firearms used in crimes, and negligent injuries due to unauthorized access. Hold My Guns’® mission is to save lives, protect property, and preserve rights.
