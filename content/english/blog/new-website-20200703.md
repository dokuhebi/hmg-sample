---
title: "July 2021 HMG Update"
date: 2021-07-03T00:00:00+06:00
author: Sarah Joy Albrecht
#image_webp: images/blog/meghna.webp
image: images/pixel.png
description : "New Website"
---

Dear friends of Hold My Guns,

I'm writing to share some exciting and much anticipated news from Hold My Guns®, our liberty-based nonprofit that partners with gun shops to offer voluntary firearms storage during times of crisis or personal need.

Before I share these updates with you, I want you to know that as I prepared to write, I felt a deep sense of gratitude for all of the support and encouragement from our community.  Like me, you have your sleeves rolled up, working in the trenches, doing your part. I appreciate you.

*Now for some good news...*

## New Website

Over Independence Day weekend, the HMG Team upgraded the [holdmyguns.org website](https://holdmyguns.org)! The site now features an interactive locations map, resources for individuals in crisis, upcoming events such as suicide prevention training for firearms industry professionals, and messages of liberty and hope from members of the firearms community. Please take a moment to add it to your bookmarks and [tell us what you think](mailto:info@holdmyguns.org).

## Firearms Storage Partners Announced

To further our efforts to work with our partners to save lives, it is my sincere delight to announce that we now offer firearms storage through partners in two states:

### Easthampton, Massachusetts

> “We are extremely proud to offer firearms storage to our community via the Hold My Guns® program. With increasing numbers of “Red Flag” laws being enacted, we saw the opportunity to provide safe, offsite firearms storage without individuals being deprived of their second amendment or due process rights.” - Christopher Graham, Co-Owner of [KC Small Arms, LLC](https://www.kcsmallarms.com/)

### Arabi, Louisiana

> “Everything we do at STBISC stems from a core belief in the US Constitution and the general principles of freedom and liberty. To that end, there has to be a way for people to store guns safely and confidently in times of need with minimal opportunity for red tape and governmental intrusion -- HMG is that option. Owned by a veteran and employing mostly veterans, STBISC has a firm understanding of and an absolute belief in the rights of Americans. We look forward to helping those who need to trust someone to take care of their guns while they take care of themselves.”-- Brannon LeBouef, USMC Veteran and Owner of [St. Bernard Indoor Shooting Center](https://www.stbisc.com/)

Crossing state lines can be an obstacle for gun owners in need of storage, as firearms laws can vary. Therefore, it is critical to the mission to reduce this barrier by partnering with gun shops in all 50 States.

## Suicide Prevention Training Offered at PA Firearms Range

In collaboration with the [Montgomery County Suicide Prevention Taskforce](https://www.montcopa.org/1439/Suicide-Prevention-Task-Force) and the [Lower Providence Rod and Gun Club](https://lprgc.org/), Hold My Guns® is offering a [QPR Institute](https://qprinstitute.com/) certified course in Suicide Prevention Gatekeeper Training.

This course is ideal for firearms instructors, range safety officers, gun shop employees, and gun owners. Students will receive a QPR booklet/guide as well as a certificate of completion that can be shared as an added credential for those offering firearms-related instruction.

### Audubon, Pennsylvania

> “We’re very pleased to offer our 2,000+ members the opportunity to attend the Question, Persuade, Refer (QPR) Suicide Prevention Gatekeeper Training. Too many have been touched with the suicide of a friend, colleague, or family member. As firearm owners, it is important that we learn how we can help those who may be experiencing suicidal thoughts.  We are especially grateful that Hold My Guns® is helping with this training.  Having an option for firearm owners to retain their firearm rights and firearms while they deal with a temporary condition is a valuable tool to assist with suicide prevention.”- Frank Tait, Chairman of the Board,  Lower Providence Rod and Gun Club  

Thank you for taking the time to read through these updates. It is my hope that they will encourage you in your journey.

With gratitude,

Sarah Joy Albrecht
