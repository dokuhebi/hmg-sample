---
title: "Branson Cerakote, LLC, Offers Voluntary Firearms Storage in Partnership with Nonprofit Hold My Guns®"
date: 2021-10-08T14:51:12+06:00
author: HMG Media
#image_webp: images/blog/meghna.webp
image: images/pixel.png
description : "This is meta description"
---

(October 8, 2021) –  Effective immediately, Branson Cerakote, LLC,  located at 131 Industrial Park Drive, Suite 3, Hollister, Missouri, 65672 , is offering voluntary firearms storage through Hold My Guns®, a liberty-based nonprofit that connects gun owners with Federal Firearms Licensees (FFLs) across the United States to provide storage during times of crisis or personal need. Customers desiring to store firearms at Branson Cerakote, LLC, are asked to email <Info@BransonCerakote.com> or call 719-896-0509 to arrange an appointment.

Founded by gun owners, Hold My Guns® is a non-legislative solution to help prevent suicide, theft of firearms, accidental shootings, and instances of stolen firearms used in crimes.

Customers can trust Branson Cerakote, LLC, to store their firearms for a variety of reasons, including to prevent unauthorized access while they are away from home such as during deployment or while on vacation.

“As a business owner who is a veteran and retired law enforcement officer, I value rights protected by the Second Amendment,” said John Siegert,  who owns Branson Cerakote, LLC, along with his wife Laura.  “Having been in the military and law enforcement, I understand that sometimes people find themselves in overwhelming situations where access to firearms can lead to devastating decisions. The Hold My Guns® program provides gun owners with the option of voluntarily storing their firearms in a secure location. We are honored to be a part of a program that supports gun rights while offering a safe alternative for responsible gun owners.”

Branson Cerakote, LLC, is known for exceptional, quality firearms customization through cerakoting and laser engraving. As firearms enthusiasts themselves, the Siegerts recognize that firearms are an investment and they take pride in their work. With nearly 10,000 subscribers on their YouTube Channel, they are well-loved in their firearms community and their artistic work inspires gun owners around the world.

John served in the US Army as an Airborne Infantryman in the 1990s.  Laura,  a graphic artist with over 20 years of experience,  also happens to be a registered nurse. Their background in service contributes to their reputation for providing consistently well-rated customer service.

“While interviewing John as part of the on-boarding process, he shared with me that, during his law-enforcement days, he would encounter situations where someone was in distress and there was a concern the individual had access to firearms,” said Sarah Joy Albrecht, Executive Director of Hold My Guns®. “John said that, as a result,  he saw good people unnecessarily get caught up in the legal system because they didn’t have a place to temporarily store firearms while they sorted things out."  

“John and Laura recognized that, by providing this option in their own community,  gun owners could voluntarily store firearms offsite while they handled their personal matters at home, be it keeping safe a teen in the household who was struggling with depression or helping a couple who was going through a volatile time in their relationship," said Albrecht.   "Storing firearms off-site at a trusted location allows time and space to handle personal matters without the temptation of access to firearms.  I applaud the leadership of John, Laura, and the Branson Cerakote team to utilize their resources in a caring way that truly supports and empowers gun owners while protecting their rights.”

For more information on how Hold My Guns® saves lives, protects property, and preserves rights, visit <https://holdmyguns.org>.

## Media Contact

![Branson Cerakote, LLC](https://holdmyguns.org/images/portfolio/branson_logo.webp)  

Branson Cerakote, LLC  
131 Industrial Park Drive, Suite 3, Hollister, Missouri, 65672  
<https://bransoncerakote.com>  
**Contact:** John Siegert - <Info@BransonCerakote.com>

Hold My Guns®  
<https://holdmyguns.org>  
**Contact:** Sarah Joy Albrecht - <info@holdmyguns.org>  
