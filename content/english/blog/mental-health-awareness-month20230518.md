---
title: "Mental Health Awareness in the Firearms Community"
date: 2023-05-18T12:34:56+06:00
author: HMG Media
#image_webp: images/blog/meghna.webp
image: images/blog/mental-health-awareness-month.png
description : "This is meta description"
---

Sharon Williston-Benadum, MA, LPCC-S, LICDC, SAP  
Hold My Guns Board Member


May is Mental Health Awareness Month and I can’t think of a better time to call out the elephant in the room: A leading cause of death is suicide.  Statistically speaking, in 2017 60% of gun related deaths in the U.S. were suicide and of the 47,173 suicides tracked for this study 51% involved a gun<sup>1</sup>. 

No, I’m not trying to start things off on a downbeat note. In fact, the exact opposite is true, so stay with me! As a mental health care professional as well as an A Girl & A Gun Certified Instructor, NRA Certified Instructor and USCCA Certified Instructor, I have the really great opportunity to talk about two of my passions; mental health awareness and firearms.  Do they really go together though?  

Recently I had the amazing opportunity to attend the A Girl and A Gun National Conference in Grand Junction, CO with 599 of my shooting sisters plus 150 instructors. Talk about a firearms community experience! Sarah Joy Albrecht, Hold My Guns Executive Director was there sharing information about Hold My Guns’ mission as well as teaching the QPR (Question, Persuade, Refer) Suicide Prevention Gatekeeper Program to participants in education blocks (if you haven’t heard about the program, please check it out at [www.qprinstitute.com](https://www.qprinstitute.com/).  
During the QPR training Sarah referred to the firearms community as a protective factor. Webster defines community as A feeling of fellowship with others, as a result of sharing common attitudes, interests, and goals. i.e., "the sense of community that firearms can provide".  

In the firearms community we look out for one another, we spend time learning together, we spend time on the range together, we are connected through our values, and we help each other be safe around firearms. When we are surrounded by community, we know each other well. We know each other’s baselines, and we purpose ourselves to encourage and help one another. It’s key to be able to recognize that someone in your community isn’t behaving in the way they normally would and be willing to have what might seem like a difficult conversation.  It’s also important to be able to recognize when we aren’t feeling like we normally would and be willing to ask for help.  
One actionable step we can all do is have a Personal Safety Plan in place. It’s important to plan ahead and not wait until a crisis when we may not be thinking clearly.  To create your own Personal Safety Plan, follow this link:  [Personal Safety Plans - Official Posts and Documents / HMG Documents - Hold My Guns Community Forum](https://community.holdmyguns.org/t/personal-safety-plans/23)

Just as we freely discuss our love for the shooting sports, let's make it our goal to freely talk about our mental health as a community. Together, we can create an environment where conversations about mental health are open, supportive, and without stigma.

<sub>1 : <https://www.pewresearch.org/fact-tank/2019/08/16/what-the-data-says-about-gun-deaths-in-the-u-s/></sub>