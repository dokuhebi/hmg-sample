---
title: "Hold My Guns: A Guardian-Angel Community Of 2A Advocates by Jack Hennessy"
date: 2024-03-01T14:51:12+06:00
author: HMG Media
#image_webp: images/blog/meghna.webp
image: images/blog/recoilmagazine.jpg
description : "This is meta description"
---

I won't reveal the entire article here [— I encourage you to read it fully on the RECOIL website at this link and leave a supportive comment —](https://www.recoilweb.com/hold-my-guns-a-guardian-angel-community-of-2a-advocates-183085.html) but I'd love to share what really stood out to me. 

We urge our FFL storage partners to find opportunities to collaborate with local first responders as part of outreach for our voluntary program.  Our FFL Partner, [Range 54 in Wichita, Kansas](https://range54.com/shoot-with-us/gun-storage/), has been outstanding in this effort, as highlighted in the article:

>Malachi Winters, age 46, is a social worker with Wichita’s Community Mental Health Crisis Center and has over 20 years of EMS experience. “The vast majority of suicide attempts are done in the heat of the moment when individuals are emotional. They’re not thinking straight. They’re distressed. And when you talk to survivors of suicide attempts, almost all of them, even in the space of like 10 minutes after the attempt, they regret it,” said Winters. “The problem with firearms is the chances of surviving a suicide attempt is so much lower than almost any other method.”
>
>Winters also works daily to promote Hold My Guns to first responders and anyone in need. It’s not a matter of constitutionality,” he said. “It’s not a matter of legality. It’s not a matter of whether or not somebody should be allowed to have a gun. What we are trying to do is tailor our therapy based off of the individual in the moment, right now.”
>
>According to Winters, those willing to consider lethal-means restriction methods while also putting in the intensive work, meeting with a licensed professional twice a week, may see significant improvement in a short amount of time.
“When people are in that space,” he said, “the majority of them are able to get out, get out of that space and back to a functioning life and get their guns back in the home in about a month.”
>
>Ken Grommet is a retired U.S. Marine and former police officer and now the co-owner of Range 54 in Wichita. Alongside his business partner, Kerry Cox, the two officially partnered with Hold My Guns in summer 2023 and made their storage facilities available for the local community.
>
>"Just because you’re having a mental crisis, it doesn’t take your rights away,” said Grommet. “The only way it becomes a problem is when you don’t recognize it, you don’t do anything about it, and then you become adjudicated. And an adjudicated person for a mental problem is the prohibitor of owning it. So we’d like to intervene before that.”
>
>Aside from participating in the Hold My Guns, program, Ken Grommet, Range 54 co-owner alongside Kerry Cox, provides various hands-on firearm and first-aid training to suit various clientele. They have also given away over 2,500 car gun safes in the past three years. Since partnering with Hold My Guns, Range 54 has accepted dozen of firearms for storage. “We have had people come in and use it,” said Grommet, “and the person who dropped them off was the same person that picked them up, which means that it would be a success story.”
>
>All Range 54 staff is trained extensively in both firearms but also suicide prevention through a program hosted by the local VA. Same as Albrecht, Grommet wants to be able to teach people how to be their own first responder — both to any physical threat but also from within. “Why wouldn’t you do something that could help?” said Grommet. “And we have the means, you know? We have the capability. We’re right here, and I just think it’s a good thing to be able to help the community out.”

Choked. Up. 

Fellow gun owners -- just every day people like you and me -- are voluntarily storing firearms through Hold My Guns, seeking help, HEALING, picking up their firearms when they're ready, and moving forward with their LIVES! 

## Media Contact

Sarah Joy Albrecht  
Founder and Executive Director  
<https://holdmyguns.org>  
<media@holdmyguns.org>  


**About Hold My Guns®** - <https://holdmyguns.org>

Hold My Guns® connects gun owners with partnering Federal Firearms Licensees (FFLs) to provide voluntary storage during times of crisis or personal need. The service is a non-legislative solution to help prevent suicide, theft, instances of stolen firearms used in crimes, and negligent injuries due to unauthorized access. Hold My Guns’® mission is to save lives, protect property, and preserve rights. 