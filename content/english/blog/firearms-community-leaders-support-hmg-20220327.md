---
title: "Firearms Community Leaders Raise Over $27,000 in Support of Nonprofit Hold My Guns®"
date: 2022-03-25T12:34:56+06:00
author: HMG Media
#image_webp: images/blog/meghna.webp
image: images/pixel.png
description : "This is meta description"
---
**For Immediate Release**

(Holdmyguns.org) - Over $27,000 was raised in support of nonprofit Hold My Guns® thanks to a surprise fundraising campaign from defense community leaders Active Self Protection (ASP), Heckler & Koch (HK), Mantis, and POM Industries. 

Hold My Guns® is a liberty-minded nonprofit that connects gun owners with partnering Federal Firearms Licensees (FFLs) to provide voluntary storage during times of crisis or personal need. 

“Stigma around seeking help for mental health in our firearms community is very real,” said Sarah Joy Albrecht, Founder and Executive Director of Hold My Guns®. “The message of hope being conveyed by ASP, HK, Mantis, and POM, carries even more value than their generous donations. Knowing that these defense champions are in the trenches as waymakers for voluntary firearms storage empowers gun owners to utilize this option with confidence.”  

Fundraising began during the National Shooting Sports Foundation’s SHOT Show in January. During the event, while Neil Weidner of Active Self Protection [interviewed Hold My Guns®’ founder](https://www.youtube.com/watch?v=Pi8Vdzolorw) Sarah Joy Albrecht on written safety plans that include securing lethal means, Weidner surprised Albrecht on camera by announcing that the four companies would each match every dollar donated up to $5,000, with a combined total of $20,000 pledged. The video aired on YouTube and viewers were encouraged to create their safety plan as well as donate.  Since its airing, two gun owners have already contacted Hold My Guns® seeking firearms storage, and many more to learn more about voluntary firearms storage.
                                                       
“Active Self Protection is honored to partner with Hold My Guns®, FFL holders, and the gun community at large to find innovative solutions to help people facing challenging situations to take courageous action to protect themselves and their loved ones,” said Stephannie Weidner, CEO of Active Self Protection (ASP), a cross-platform safety education brand, with over 2.56 million subscribers on YouTube.  

“The Active Self Protection team are Second Amendment absolutists and we strongly believe we should be able to use firearms for their intended purpose free of government interference. This requires us to be proactive and purposeful in finding ways to support the very real people in our community when they are facing change and potential crises,” said Weidner. She added on a personal note, “Those of us that are followers of Jesus should also be concerned with how we can truly love and support brave people in crisis as they do the right thing. Hold My Guns® is deeply involved in both truly caring for people and also finding the solutions to protect our freedoms and liberty and I’m thrilled to be a small part of that.”

Firearms manufacturer Heckler & Koch USA’s Marketing Director Bill Dermody noted that anyone can find themselves in unexpected situations that can affect mental health, gun owners included. 

"At HK we are so happy to be able to help out, in a small way, with the good work being done at Hold My Guns®,” Dermody said. “Mental health is one of those issues that has affected just about everyone, including friends and colleagues here at HK. We couldn’t be more proud to be working with a network of such great Americans. They are passionate advocates for mental health, with an understanding and respect for the culture of the legally armed citizen. Keep up the good work!"
                               
Safe handling of firearms and taking seriously the responsibility of proficiency training is a founding principle for Mantis. Their popular products use motion-sensing technology that affix to firearms and provide real-time feedback through their app, which includes scenario-based training sequences, so that gun owners can hit targets with data-driven accuracy. The company’s booth served as the backdrop for ASP’s SHOT Show video. 

 "We're 100% aligned on the mission to support HMG in their efforts to support responsible gun ownership,” said Mantis President and COO Austin Allgaier. “When we exercise individual responsibility, the entire community benefits.  We're thrilled to see such a positive response to HMG and are excited to see their growth continue." 

Feeling vulnerable from a personal defense standpoint is an understandable concern for gun owners deciding to use offsite firearms storage. Having tools in ones’ arsenal such as oleoresin capsicum (aka pepper spray), can help offset these concerns during a suicidal crisis in the household when having access to firearms is not an option.

“POM Industries is committed to empowering people to live life to its fullest,” said Alexander Caruso, Founder and CEO at POM. “Our name, which stands for “Peace of Mind”,  underscores how having the right resources can make a difference not only in personal defense but in one’s outlook. POM Industries financially supports the mission of Hold My Guns® because we want to ensure that our community has the option of voluntary firearms storage, and the peace of mind that goes with knowing firearms are secured in a professional, stigma-free environment.”              
                                      
Combined with the $7,359 donated by individuals supporting Hold My Guns®, matching donations brough the campaign total to $27,359. A portion of the donations are already being used to create much-needed brochures and firearms package inserts to be distributed to gun owners at firearms retail locations, gun shows, and to interested firearms storage partners.

## Media Contact

Sarah Joy Albrecht  
Founder and Executive Director  
<https://holdmyguns.org>  
<media@holdmyguns.org>  


**About Hold My Guns®** - <https://holdmyguns.org>

Hold My Guns® connects gun owners with partnering Federal Firearms Licensees (FFLs) to provide voluntary storage during times of crisis or personal need. The service is a non-legislative solution to help prevent suicide, theft, instances of stolen firearms used in crimes, and negligent injuries due to unauthorized access. Hold My Guns’® mission is to save lives, protect property, and preserve rights. 

**About Heckler & Koch** - <https://hk-usa.com> 

Heckler & Koch is the world’s premier small arms systems company and a major supplier to global military, law enforcement agencies, and civilian shooters. An innovative leader in design and manufacturing, Heckler & Koch provides technologically advanced firearms, logistical support, training, and specialized services with the highest standards of innovation and reliability to its customer base. Heckler & Koch’s well-known range of products include the USP series pistols, MP5 submachine gun, the MP7 Personal Defense Weapon, the G36 weapon system, the HK416 enhanced carbine, HK45, P30, and most recently VP series pistols.

**About POM Industries** - <https://pompepperspray.com>

POM Industries leverages over 40 years of industry experience in the design and manufacturing of aerosol defense products. They have improved the safety, performance and design of pepper spray to offer customers the most up-to-date and powerful non-lethal defense product on the market.

**About Active Self Protection** - <https://activeselfprotection.com>

Active Self Protection (ASP) was founded in 2011 by John Correia, a nationally-recognized subject matter expert in private citizen defensive encounters and law enforcement use of force. Through its cross-platform educational videos and training courses, ASP aims to teach people in all walks of life to develop the attitude, skills, and plan to defend themselves and their families from harm.

**About Mantis** - <https://mantisx.com>

Mantis is on a mission to improve every shooter at every level. Regardless if a shooter is a novice or a world champion, a military recruit or special forces, Mantis provides tech-driven products that accelerate the accuracy improvement process faster than its competitors. Using objective data to analyze markers not visible to the human eye, Mantis products train shooters’ neural pathways with quantitative and qualitative information to unlock rapid improvement, consistent performance, and perfect practice. 
