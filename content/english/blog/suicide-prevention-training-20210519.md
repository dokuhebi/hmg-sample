---
title: "Hold My Guns® to Offer Suicide Prevention Training at PA Gun Range"
date: 2021-05-19T14:51:12+06:00
author: Sarah Joy Albrecht
#image_webp: images/blog/meghna.webp
image: images/blog/QPR.jpg
description : "This is meta description"
---

**This course is ideal for firearms instructors, range safety officers, gun shop employees, and gun owners. Students will receive a QPR booklet/guide as well as a certificate of completion that can be shared as an added credential for those offering firearms-related instruction.**

How do you help someone who is experiencing suicidal thoughts? What are some of the risk factors for suicide that may be less obvious? In this course, attendees will learn the QPR approach for helping to prevent suicide. Learn to recognize the warning signs of suicidality, how to respectfully ask someone if they are experiencing suicidal thoughts, as well as how to offer help and connect folks to appropriate support resources.

## About the trainers

Anna Trout is the Crisis and Diversion Director for the Montgomery County Office of Mental Health. Her work includes partnering with Police Departments, Hospitals, and the MontCo Suicide Prevention Taskforce.

Officer Andrew Parkins is an Officer for Upper Providence Police Department and is a MontCo Suicide Prevention Taskforce member.

Both Anna and Officer Parkins are very passionate about their work. They are both certified as QPR trainers through the [QPR Institute](https://qprinstitute.com/).

## Hosted by

Hold My Guns®, a for-gun-owners by-gun-owners 501(c)(3) non-profit that partners with FFLs to provide voluntary off-site firearms storage during times of crisis or personal need (such as during deployment). Learn more at <https://holdmyguns.org>.

The Montgomery County Suicide Prevention Taskforce. For more information about local suicide prevention efforts and resources please visit: <https://www.montcopa.org/suicidepreventiontaskforce>.

[Lower Providence Rod & Gun Club](https://lprgc.org/) **NOTE:** LPRGC Members Paul and Genevieve Jones serve on the HMG Board of Directors. We are also grateful to Frank Tait for his help to organize this important event.

*The 7/26 class is limited to LPRGC member families. Pre-registration required. At the time of this post, only 15 of 50 spots remain.*
