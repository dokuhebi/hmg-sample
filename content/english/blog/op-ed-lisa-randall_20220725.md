---
title: "Being the “little guy” within a controversial conversation"
date: 2022-07-24T14:51:12+06:00
author: Lisa Randall
image_webp: images/blog/lisa_randall_op_ed.webp
image: images/blog/lisa_randall_op_ed.jpg
description : "This is meta description"
---

Within the last few months, we have seen the tragic events unfold in Buffalo and Uvalde, like everyone we watched in horror the details unfold in the media.  We hugged our children tighter; we called our grandparents to check in more often and as a small family firearms company we did something else.

We began to carry the weight of our local communities’ fears and anger. Opportunities once open to us to work within the community were taken away “in an attempt to not upset people and be more inclusive.”  Words slung at us for restoring hundred-year-old heirlooms as “killers” because people needed to place their anger somewhere. Sitting at our kitchen table, my husband and I wondered if being a smaller company was easier to become a target because people know the direct line to the top… the top of a two-man organization as opposed to a giant juggernaut.

Here in Washington State, we have very strict gun laws with more going into effect within the month.  It became clear that those so angry for change, do not even know the basics of those laws or that they even exist. When we decided to partner with Hold My Guns, it was a decision we made whole-heartedly as we ourselves have grieved after a firearms-related death in our family. One that maybe would have been preventable had the option been in place for safe storage without judgment. We knew that we had to make this opportunity available to others after our loss, to hopefully prevent someone else’s tragedy.

People want real change; they want to make a real difference, but it's hard to go outside their comfort zone to do the work. How do we find what we have lost as a community?  How do we stop moving forward at the speed of light? Why can we no longer pause and look behind us to see what we have lost?   I often have a lot of people speak to me regarding training, they approach me with this conversation covertly because they are fearful that their friends and neighbors might find out that they are even interested in self-protection. Have we gone so far in one direction that personal responsibility has been so lost and even fearful to speak out against the mob?

Creating room to have the conversation about Hold My Guns, training or even an enthusiastic debate without judgment has been exhausting but it is the right thing to do. As a small company, every day we wake up, we work hard for our family, our values and to try to make an impact. Little did we know that we would be such a hot button within our own community for merely the word “firearm”. Within the last year in our city alone, we have had some legacy firearm shops close. I am sure it’s been a mix of politics, pandemic, burn out and just a personal season closing. We try our best to carve out opportunities to teach, to talk, to help people understand that they do not need to be fearful of a tool.  They do not need to be a victim of what is going on around them.

Carrying the weight of people’s anger has been something new to navigate.  As a mother of elementary school children, having to tell my seven-year-old to run, fight, hide in that order was gutting.  But hearing others avoiding the conversation completely to the point of “we don’t talk about firearms at all” is even scarier.  This is how we create environments for accidental shootings, environments of fearful young adults too afraid to say anything and people so buried into their personal realities, they stop looking around to what is happening in front of them.   We must be more for each other as people, as communities.   Going beyond our own porches, outside our walls to meet our neighbors, invest in those who isolate, make the time to call, write, stop-by, have lunch or just to say, how are you?

As a powerful team of two, we will continue to wake up and do the best we can for our family and to continue forward in talking.  Even to those who do not want to listen, we will continue to press forward as a balanced voice.   We will continue to hold the weight of our communities’ fear and anger, the “what can be done” and the “how dare you”. We will hold this with grace and try to speak so those people can hear. If the pandemic and events of the world have shown us, if you cannot run into a situation without judgment then you are part of the challenge.

So, we’ll be here… the “little guys” and keep going, keep our porchlight on and walk aside our neighbors even in their fears.
