---
title: "KC Small Arms Partners with Nonprofit Hold My Guns® to Offer Firearms Storage"
date: 2021-06-04T14:51:12+06:00
author: HMG Media
#image_webp: images/blog/meghna.webp
image: images/pixel.png
description : "This is meta description"
---

(June 3, 2021) –  Effective immediately, KC Small Arms, 412 Main Street, Easthampton, MA, is offering voluntary firearms storage through Hold My Guns®, a liberty-based nonprofit that connects gun owners with Federal Firearms Licensees (FFLs) across the United States to provide storage during times of crisis or personal need. Customers desiring to store firearms at KC Small Arms are asked to call (413) 284-4739 to arrange an appointment.

“We are extremely proud to offer firearms storage to our community via the Hold My Guns® program,” said Christopher Graham, Co-Owner of KC Small Arms, LLC. “With increasing numbers of “Red Flag” laws being enacted, we saw the opportunity to provide safe, offsite firearms storage without individuals being deprived of their Second Amendment or due process rights.”

Founded by gun owners, Hold My Guns® is a non-legislative solution to help prevent suicide, theft, instances of stolen firearms used in crimes, and accidental shootings. Partnering gun shops make a difference in their communities by utilizing their firearms storage space to help save lives.  KC Small Arms in Massachusetts is the nonprofit’s first storage partner.

“KC Small Arms has a long-standing reputation for offering quality, innovative products, and exemplary customer service. It is with gratitude that we announce that KC Small Arms is our first official storage partner,” said Sarah Joy Albrecht, Founder and President of Hold My Guns®. “KC Small Arms’ clients are always treated with utmost respect and dignity, and offering voluntary firearms storage is a practical extension of these values. They truly do care.”

Gun owners have a variety of personal reasons, including preventing unauthorized access to firearms while away on vacation or during military deployment, as to why they would choose to store their firearms outside of their homes.  However, not everyone has a friend or family member who can lawfully help with firearms storage. This service is a life-saving option that fills this gap.

## Media Contact

![KC Small Arms](https://holdmyguns.org/images/portfolio/kc-small-arms.png)  
KC Small Arms  
<https://www.kcsmallarms.com>  
Contact: Christopher Graham  
<sales@KCSmallArms.com>  

Hold My Guns®  
<https://www.holdmyguns.org>  
Contact: Sarah Joy Albrecht  
<info@holdmyguns.org>  
