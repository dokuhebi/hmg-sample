---
title: "Veteran-Owned Wyoming Gun Shop Offers Voluntary Storage in Partnership with Hold My Guns"
date: 2022-06-22T12:34:56+06:00
author: HMG Media
image_webp: images/blog/gold-spur-logo.webp
image: images/blog/gold-spur-logo.jpg
description : "This is meta description"
---

#### For Immediate Release

(Holdmyguns.org) -  [Gold Spur Outfitters, LLC](https://goldspuroutfitters.com), (GSO) located in Laramie, WY, is offering voluntary firearms storage in partnership with 501(c)(3) nonprofit Hold My Guns® (HMG). Customers desiring storage are asked to drop in during normal business hours.  

Hold My Guns®, a for-gun-owners by-gun-owners organization, connects gun owners with Federal Firearms Licensees (FFLs) across the United States to provide storage during times of crisis or personal need, such as during deployment or the sale of a home. The referral service helps to prevent suicide, negligent injuries, theft of firearms, and instances of stolen firearms used in crimes.

“Gold Spur Outfitters, LLC, is proud to partner with Hold My Guns®, to provide our community and surrounding areas, with secure, affordable firearms storage,” said Lloyd Baker, GSO’s Owner and CEO. “We are committed to every American’s Second Amendment and property rights, and offer our resources in a way that provides a voluntary, non-legislative solution to keep people and property safe. We at Gold Spur Outfitters believe that there are many reasons someone may want to store their firearms with us, including during a personal crisis. We are here to help.”

In addition to firearms sales and storage, Gold Spur Outfitters, LLC, offers many services including the restoration of heirloom firearms, gunsmithing and customization, and precision optics-mounting services for unparalleled shot accuracy.

“Guns are common household items, and gun ownership is a normal part of American life,” said Sarah Joy Albrecht, Founder and Executive Director of Hold My Guns®. “According to the 2021 National Firearms Survey, 32% of the U.S. adult population ages 18 and over own firearms - this translates to about 81.4 million Americans.  Prioritizing accessibility to voluntary firearms storage empowers gun owners to responsibly manage their personal matters without stigma or fear of losing their Constitutionally-protected rights.  Not everyone is blessed to have friends or family who can help meet this need, and our service fills the gap. Veteran-owned and operated Gold Spur Outfitters, LLC, has joined HMG’s growing family of partnering FFLs, and we are thrilled to share this news with our beloved firearms community.”

For more information on how Hold My Guns® saves lives, protects property, and preserves rights, and to support the mission, visit <https://holdmyguns.org>.

## Media Contact

Sarah Joy Albrecht  
Founder and Executive Director  
<https://holdmyguns.org>  
<media@holdmyguns.org>  

**About Hold My Guns®** - <https://holdmyguns.org>

Hold My Guns® connects gun owners with partnering Federal Firearms Licensees (FFLs) to provide voluntary storage during times of crisis or personal need. The service is a non-legislative solution to help prevent suicide, theft, instances of stolen firearms used in crimes, and negligent injuries due to unauthorized access. Hold My Guns’® mission is to save lives, protect property, and preserve rights.
