---
title: "Become a Storage Partner: On-boarding Process"
date: 2020-04-05T14:51:12+06:00
author: Sarah Joy Albrecht
#image_webp: images/blog/meghna.webp
image: images/pixel.png
description : "This is meta description"
---

Welcome! We are here to support you in this process. If at any time you’d like to speak on the phone, please let us know and we can set up a time to chat. We may have referrals and resources available, should you run into any obstacles completing the requirements, so please reach out if you need us!

After you have have contacted us to let us know of your interest, a member of our team will welcome you and will respond to any questions you have and provide a link to a brief intake survey regarding your FFL and your storage capabilities.

As our legal documents are propriety, we require that interested parties sign a Non-Disclosure Agreement (NDA).

Upon receipt of the signed NDA, we will send for your review the Storage Partner Agreement, as well as a customer-facing Storage Template that contains a waiver of liability, a place for the description of firearms being stored to correspond with your A&D Logbook, and blank spaces for you to work with your team to insert your in-house policies regarding firearms storage that are compliant with your state and local laws.

Please note that reviewing these documents does not obligate you to sign on as a storage partner. We value transparency, and want our storage partners to know what is expected of them before they decide to move forward. There are no imposed fees from HMG to become a storage partner, and storage partners may terminate their agreement at their discretion, with notification and procedures to protect storage customers as outlined in the agreement.

In addition to the completed Storage Partner Agreement and modified Customer-Facing Storage Agreement, a simple letter from your attorney attesting that your Customer-Facing Storage Agreement is compliant with your state and local laws, as well as verification that Hold My Guns has been added as “additional insured” to your policy are the final requirements to green-light you as a storage partners and list your FFL on our website.

As restrictions from COVID-19 lift, we will explore the possibilities of launch parties to help announce your service in your community. We will also help connect you to pathways for referrals to your FFL.

Our website will grow to spotlight our storage partners and the good things they are doing in their respective communities. We are excited to send positive traffic your way!
